import Router from "./Router.js";
import "./Prototypes/ObjectProto.js";
import "./Prototypes/StringProto.js";
import {type_check} from "./Helpers.js";

export default class React {

    /**
     * @param {HTMLElement}
     * @private
     */
    _rootElement;

    /**
     * @param {Router}
     * @private
     */
    _router;

    /**
     * @param {Dom}
     * @private
     */
    _dom

    get dom() {
        return this._dom;
    }

    set dom(value) {
        this._dom = value;
    }

    constructor() {
        this._router = new Router(this);
    }

    get rootElement() {
        return this._rootElement;
    }

    set rootElement(el) {
        this._rootElement = el;
        this._router.renderRoute();
    }

    get router() {
        return this._router;
    }

    set router(value) {
        this._router = value;
    }

    /**
     *
     * @param element
     * @returns {null|WebAssembly.TableKind|*}
     */
    render = (element) => {
        if (!element.children) return element.element;
        const root = element.element;

        element.children.forEach((child) => {
            if (child.children) {
                child.children.forEach((childElement) => {
                    let renderedElement = this.render(childElement)
                    child.element.appendChild(renderedElement)
                })
            }
            root.appendChild(child.element);
        })

        if (this._rootElement !== undefined) this._rootElement.appendChild(root);

        return root;
    }
}
