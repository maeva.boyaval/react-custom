import Dom from "./Dom.js";
import Home from "../Components/Home.js";

export default class Router {

    routes = [];
    mode = null;
    rootUrl = "/";

    /**
     * @type {React}
     */
    react = null;

    /**
     * @type {Dom}
     */
    dom = null;

    constructor(react, options = {}) {
        this.mode = window.history.pushState ? "history" : "hash";
        this.history = window.history;
        this.react = react;

        this.dom = new Dom(react);

        if (options.mode) this.mode = options.mode;
        if (options.root) this.rootUrl = options.root;

        this.loadRoutes(this.dom);
    }

    /**
     * Adds a new route to use
     * @param {string} path
     * @param {HTMLElement} component
     * @returns {Router}
     */
    addRoute = (path, component) => {
        this.routes.push({path, component});
        return this;
    };

    /**
     * Removes the route
     * @param {string} path
     * @returns {Router}
     */
    removeRoute = (path) => {
        for(let i=0; i<this.routes.length; i++) {
            if(this.routes[i].path === path) {
                this.routes.slice(i, 1);
                return this;
            }
        }
        return this;
    };

    /**
     * Clears the routes
     * @returns {Router}
     */
    clear = () => {
        this.routes = [];
        return this;
    };

    /**
     * Remove the first and the last /
     * @param {string} path
     * @returns {string}
     */
    clearSlashes = (path) => path.toString().replace(/\/$/, "").replace(/^\//, "");

    /**
     * Changes the location
     * @param {string} path
     * @returns {Router}
     */
    push = (path = "") => {
        if (this.mode === "history") {
            window.history.pushState(null, null, this.rootUrl + this.clearSlashes(path));
            this.renderRoute();
        } else {
            window.location.href = `${window.location.href.replace(/#(.*)$/, "")}#${path}`;
        }
        return this;
    }

    goBack = () => {
        this.history.back();
        this.renderRoute();
    }

    goForward = () => {
        this.history.forward();
        this.renderRoute();
    }

    /**
     * Gets the current path
     * @returns {string}
     */
    getFragment = () => {
        let fragment;

        if (this.mode === "history") {
            fragment = this.clearSlashes(decodeURI(window.location.pathname + window.location.search));
            fragment = fragment.replace(/\?(.*)$/, "");
            fragment = this.rootUrl !== "/" ? fragment.replace(this.rootUrl, "") : fragment;
        } else {
            const match = window.location.href.match(/#(.*)$/);
            fragment = match ? match[1] : "";
        }
        return this.clearSlashes(fragment);
    };

    /**
     *
     */
    renderRoute = () => {
        if (this.current === this.getFragment()) return;
        this.current = this.getFragment();

        this.routes.some(route => {
            const match = this.current === route.path;

            if (match) {
                this.dom.render(route);

                const links = document.querySelectorAll("a.link")
                links.forEach(link => link.addEventListener('click', (e) => {
                    e.preventDefault()
                    this.push(e.currentTarget.getAttribute("href"))
                }))
                return match;
            }
            return false;
        });
    };

    /**
     *
     * @param dom
     */
    loadRoutes = (dom) => {
        [
            {
                "component": Home,
                "path": ""
            }, {
                "component": () => {
                    console.log("Route 2");
                    return dom.createHtmlElement("div", {test: "test"}, "Hello World from page 2", 
                        dom.createHtmlElement("button", {"click": () => this.push("/"), "class" : 'link'}, "Back")
                    )
                },
                "path": "url2"
            }
        ].forEach(route => this.addRoute(route.path, route.component))
    }
}
