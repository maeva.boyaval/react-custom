import {events, isClass, type_check, type_check_v2} from "./Helpers.js";

export default class Dom {

    /**
     * @type {React}
     */
    _react;

    get react() {
        return this._react
    }

    set react(value) {
        this._react = value
    }

    /**
     *
     * @type {null|Component}
     * @private
     */
    _currentComponent = null;

    get currentComponent() {
        return this._currentComponent;
    }

    set currentComponent(value) {
        this._currentComponent = value;
    }

    _rendered

    get rendered() {
        return this._rendered;
    }

    set rendered(value) {
        this._rendered = value;
    }

    constructor(react = null) {
        this.react = react;
    }


    /**
     * Clears the page
     */
    removeDom = () => {
        if (this.rendered && this.rendered.element) {
            this.rendered.element.childNodes.forEach((ele) => {
                console.log(ele)
                this.rendered.element.removeChild(ele)
            });


            this.rendered.element.innerHTML = ""
            this.rendered.element.innerText = ""

        }
    }

    /**
     *
     * @param route
     */
    render(route) {
        this.removeDom();
        let element;

        if (isClass(route.component)) element = this.generateComponent(route.component, route.props);
        else element = route.component();

        this.rendered = element;
        this.react.render(element);
    }

    /**
     * @param component
     * @param prevRendered
     */
    updateDom(component, prevRendered) {
        this.removeDom()

        let indexToChange;

        this.rendered.children.forEach((child, index) => {
            console.log(prevRendered.type_check({value: child}))
            if (prevRendered.element.isEqualNode(child.element)) {
                console.log(index)
                indexToChange = index
            }
        })

        if (indexToChange) {
            this.rendered.children[indexToChange] = component.rendered
        }

        console.log(this.rendered)
        this.react.render(this.rendered);

    }

    /**
     * @param {string} tagOrComponent
     * @param {object} props
     * @param {...object|string} children
     * @returns {object}
     */
    createHtmlElement(tagOrComponent, props, ...children) {
        let html = {element: null, children: []};

        if (type_check(tagOrComponent, "string")) {
            let element = document.createElement(tagOrComponent);
            if (props) this.addAttributes(element, props)
            html.element = element;
        } else {
            if (type_check(tagOrComponent, 'function')) {
                html = this.generateComponent(tagOrComponent, props, children)
            }
        }

        this.createChildren(children, html)

        return html;
    }

    /**
     *
     * @param componentClass
     * @param props
     * @param children
     * @returns {*}
     */
    generateComponent(componentClass, props, ...children) {
        let component = new componentClass()
        this.currentComponent = component
        component.dom = this
        component.receiveData(props)
        component.rendered = component.render()
        this.createChildren(children, component.rendered)
        return component.rendered
    }

    /**
     *
     * @param childrenElement
     * @param parentElement
     */
    createChildren(childrenElement, parentElement) {

        for (let child of childrenElement) {
            let childElement;

            if (type_check(child, "null")) break;
            if (type_check(child, 'object') && child.tagOrComponent) {
                const {tag, props, children} = child
                childElement = {parent: parentElement, ...this.createHtmlElement(tag, props, children)}
            } else if (type_check(child, 'object') && child.element) {
                childElement = {...child}
            } else if (type_check(child, 'string') && !type_check(child, 'object')) {
                childElement = {element: document.createTextNode(child.interpolate(this.currentComponent)), parent: parentElement}
            }

            if (typeof childElement !== "undefined") {
                parentElement.children.push(childElement);
            }
        }
    }

    /**
     *
     * @param element
     * @param props
     */
    addAttributes(element, props) {
        Object.keys(props).map((attribute) => {
            if (events.find(event => event === attribute)) {
                element.addEventListener(attribute, props[attribute])
            } else {
                element.setAttribute(attribute, props[attribute])
            }
        });
    }
}
